#include "readXML.h"


XMLparser::XMLparser(std::string filename) {
	pf = new std::ifstream(filename); 
}

XMLparser::~XMLparser() {
	pf->close();
	delete pf;
}

std::string XMLparser::getValue(std::string query)
{
	int len = query.length(); 
	char c = NULL;
	char *temp = (char *)malloc(sizeof(char)*len);
	memset(temp, 0x00, len);
	std::string value;


	bool isStartWrite = false;
	bool writed = false;
	bool compared = false;
	int i =0;

	while(!pf->eof())
	{
		c = pf->get();
		if(compared) 
		{ 
			temp[i] = c; 
			if( i >= len-1) { compared = false; temp[i+1] = '\0';}
			i +=1;
		}
		if(c == '<' && !writed) { writed = false; compared = true;} else if (c == '<' && writed) { value += '\0'; break;}
		if(writed) { 
			value += std::string(1, c);}
		if(isStartWrite && c == '>') { 
			writed = true; }
		if( strcmp(temp, query.c_str()) == 0) { 
			isStartWrite = true;
		}else if(temp[len] == '\0' && !compared){
			i =0;
		}
	
	}
	
	return value;



}